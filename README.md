# README #

Flood\Component\Container

`composer require flood/component-container` - when using as module in an existing app

`composer create-project flood/component-container` - when using standalone

[BitBucket](https://bitbucket.org/bemit_eu/floodcomponent-container/src/master/)
[master zip](https://bitbucket.org/bemit_eu/floodcomponent-container/get/master.zip)

# Usage
Flood\Component\Container provides the main logic for other Containers used in flood/* parts.

# Licence
This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Guide

At [painttheweb.de/flood-component/container](https://painttheweb.de/flood-component/container) is the component explained.

# Copyright

    2017 - 2018 | bemit UG (haftungsbeschränkt) - project@we.bemit.eu