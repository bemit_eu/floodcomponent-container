<?php

spl_autoload_register(function ($class) {
    $prefix = 'Flood\Component\Container\\';
    $prefix_length = strlen($prefix);
    if (strncmp($prefix, $class, $prefix_length) !== 0) {
        return;
    }
    $src_path = 'src/';
    $class_path = __DIR__ . '/' . $src_path . str_replace('\\', '/', substr($class, $prefix_length)) . '.php';
    if (file_exists($class_path)) {
        require $class_path;
    }
});