<?php

namespace Flood\Component\Container;

/**
 * Flood\Component\Container interface
 *
 * @category
 * @package    \Flood\Component\Container
 * @author     Michael Becker - michael@bemit.codes
 * @link       https://painttheweb.de/flood-component/container
 * @copyright  2017 bemit UG (haftungsbeschraenkt)
 * @since      Version 0.0.1
 * @version    0.5.0
 */
interface iContainer extends \Psr\Container\ContainerInterface {
    /**
     * @return Container|iContainer
     */
    static function i();

    static function overwriteContainer($callback);

    function destroy();

    function get($id);

    function register($id, $callable);

    function &bind($id, &$reference, $overwrite);

    function overwrite($id, $callable);

    function isOverwritten($id);

    function has($id);
}