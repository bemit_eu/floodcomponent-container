<?php

namespace Flood\Component\Container;

/**
 * Flood\Component\Container
 *
 * @todo       recode overwriting logic, so overwrite uses a handler_list and not just $i
 *
 * @package    \Flood\Component\Container
 * @author     Michael Becker - michael@bemit.codes
 * @link       https://painttheweb.de/flood-component/container
 * @copyright  2017 bemit UG (haftungsbeschraenkt)
 * @since      Version 0.0.1
 * @version    0.5.1
 */

class Container implements iContainer {

    public static $debug;
    /**
     * @var null|self|object contains the singleton object of this class, when overwritten with `overwriteContainer` that object is used
     */
    protected static $i = null;
    public $container_id = '';
    /**
     * @var array
     */
    protected $object_list = [];

    protected function __construct() {
        // protected to disallow direct initiation
    }

    /**
     * Returns an instance of this class
     *
     * @return self
     */
    public static function i() {
        if(null === static::$i) {
            if(get_class() !== get_called_class()) {
                $tmp = get_called_class();
                static::$i = new $tmp;
            } else {
                static::$i = new self;
            }
        }

        return static::$i;
    }

    /**
     * Specifies an object that should be used instead of this class, that that container works perfectly it must
     * 1. be a child from this class/implements iContainer
     * 2. have a method that returns an object, that implements iContainer.
     *
     * @param callable $callback the new container that is then used, the callable needs to return an object
     */
    public static function overwriteContainer($callback) {
        static::$i = call_user_func($callback);
    }

    /**
     * Destroys the active container object, thous all registered objects are deleted
     */
    public function destroy() {
        static::$i = null;
    }

    /**
     * returns the executed callback for that `id`, or the value
     *
     * @param string $id
     *
     * @return mixed
     */
    public function get($id) {
        try {
            if($this->has($id)) {
                // todo: callable call and class instantiation with params and reflection and di
                if(
                    is_object($this->object_list[$id]['callable']) &&
                    ($this->object_list[$id]['callable'] instanceof \Closure) &&
                    (
                        (isset($this->object_list[$id]['executed']) && $this->object_list[$id]['executed'])
                        || !isset($this->object_list[$id]['executed'])
                    )
                ) {

                    return call_user_func($this->object_list[$id]['callable']);
                } else if(
                    isset($this->object_list[$id]['executed']) && !$this->object_list[$id]['executed']
                ) {
                    $this->object_list[$id]['executed'] = true;

                    return $this->object_list[$id]['callable'] = call_user_func($this->object_list[$id]['callable']);
                } else {
                    return $this->object_list[$id]['callable'];
                }
            } else {
                throw new NotFoundException('object not found: ' . $id);
            }
        } catch(NotFoundException $e) {
            if(static::$debug) {
                echo 'FloodContainer Exception: ' . $e->getMessage() . "\r\n";
            } else {
                error_log('FloodContainer Exception: ' . $e->getMessage());
            }
            exit(1);
        }
    }

    /**
     * if the `id` exists and its `callable` is not empty
     *
     * @param string $id
     *
     * @return bool
     */
    public function has($id) {
        if(isset($this->object_list[$id]) && !empty($this->object_list[$id]['callable'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * registers the callback or the value for that `id` when not overwritten or doesn't exist
     *
     * @param string         $id
     * @param callable|mixed $callable
     * @param bool           $executed `true` = calling callable on every `get($id)`, `false` = calling callable one time at `get($id)` [singleton], `null` = just return callable on `get($id)`; if the callable should be executed one time and the result used in `get()` in further times (lazy execution when false, uses as registered when true)
     */
    public function register($id, $callable, $executed = true) {
        if(
            ($this->has($id) && !$this->isOverwritten($id)) ||
            !$this->has($id)
        ) {
            $this->object_list[$id]['callable'] = $callable;
            $this->object_list[$id]['overwrite'] = false;
            $this->object_list[$id]['executed'] = $executed;
        }
    }

    /**
     * if the `id` is overwritten already, or was bound with overwrite option
     *
     * @param $id
     *
     * @return bool
     */
    public function isOverwritten($id) {
        if(isset($this->object_list[$id]['overwrite']) && true === $this->object_list[$id]['overwrite']) {
            return true;
        }

        return false;
    }

    /**
     * Bind any executable thing to one container `id` to let that be returned, now use multiple truly the same things in multiple containers/modules
     *
     * @param                      $id
     * @param null|Object|callable $reference
     * @param bool                 $overwrite
     *
     * @return bool|mixed
     */
    public function &bind($id, &$reference = null, $overwrite = false) {
        if(null !== $reference) {
            // when $reference is not null the function is a setter
            if(
                ($this->has($id) && !$this->isOverwritten($id)) ||
                !$this->has($id)
            ) {
                // when id exist and is not overwritten AND
                // when id does not exist
                $this->object_list[$id] = &$reference;
                $this->object_list[$id]['overwrite'] = $overwrite;
                $this->object_list[$id]['bind'] = true;

                return $this->object_list[$id]['bind'];
            } else {
                $this->object_list[$id]['bind'] = false;

                return $this->object_list[$id]['bind'];
            }
        } else {
            if($this->has($id)) {
                // when $reference is null the function is a getter, but only when the object exists
                return $this->object_list[$id];
            } else {
                return false;
            }
        }
    }

    /**
     * overwrites the callback for that `id`
     *
     * @param string         $id
     * @param callable|mixed $callable
     * @param bool           $executed
     */
    public function overwrite($id, $callable, $executed = true) {
        $this->object_list[$id]['callable'] = $callable;
        $this->object_list[$id]['overwrite'] = true;
        $this->object_list[$id]['executed'] = $executed;
    }
}