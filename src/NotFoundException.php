<?php

namespace Flood\Component\Container;

class NotFoundException extends \Exception implements \Psr\Container\NotFoundExceptionInterface {
}